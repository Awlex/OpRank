defmodule OpRank.Repo.Migrations.AddSeasonToAnime do
  use Ecto.Migration

  def change do
    alter table(:anime) do
      add :mal_id, :integer
      add :season, :string
      add :year, :integer
    end

    create index(:anime, [:season, :year])
  end
end
