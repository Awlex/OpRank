defmodule OpRank.Repo.Migrations.CreateAccountSettings do
  use Ecto.Migration

  def change do
    create table(:account_settings, primary_key: false) do
      add :user_id,
          references(:users, on_delete: :delete_all, on_update: :update_all, type: :binary_id),
          primary_key: true

      add :key, :string, primary_key: true
      add :value, :bytea
    end
  end
end
