defmodule OpRank.Repo.Migrations.CreateVotes do
  use Ecto.Migration

  def change do
    create table(:votes, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :winner, references(:themes, on_delete: :delete_all, on_update: :update_all, type: :binary_id)
      add :loser, references(:themes, on_delete: :delete_all, on_update: :update_all, type: :binary_id)
      add :user_id, references(:users, on_delete: :delete_all, on_update: :update_all, type: :binary_id)

      timestamps(type: :utc_datetime)
    end

    create index(:votes, [:winner])
    create index(:votes, [:loser])
    create index(:votes, [:user_id])
    create unique_index(:votes, ["least(winner, loser)", "greatest(winner, loser)"], name: :unique_votes)
    create constraint(:votes, :different_themes, check: "winner != loser")
  end
end
