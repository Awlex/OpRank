defmodule OpRank.Repo.Migrations.FixUniqueVotes do
  use Ecto.Migration

  def change do
    drop unique_index(:votes, ["least(winner, loser)", "greatest(winner, loser)"], name: :unique_votes)
    create unique_index(:votes, [:user_id, "least(winner, loser)", "greatest(winner, loser)"], name: :unique_votes)
  end
end
