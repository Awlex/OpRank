defmodule OpRank.Repo.Migrations.PrepareFullTextSearchForThemes do
  use Ecto.Migration

  #   Original idea
  def up do
    execute """
    CREATE MATERIALIZED VIEW theme_search_index as
      SELECT
        themes.id,
        themes.name as theme_name,
        anime.name as anime_name,
        type,
        index,
        setweight(to_tsvector(themes.name), 'A') ||
          setweight(CASE
            WHEN type = 'op' THEN to_tsvector('english', 'op') || to_tsvector('english', 'opening')
            WHEN type = 'ed' THEN to_tsvector('english', 'ed') || to_tsvector('english', 'ending')
          END, 'C') ||
        setweight(to_tsvector(index::varchar), 'C') as document
      FROM themes
      join anime on anime_id = anime.id
    """

    execute """
    CREATE MATERIALIZED VIEW anime_search_index as
      SELECT id, name, to_tsvector(name) as document FROM anime
    """

    create index(:theme_search_index, [:document], using: :gin)
    create index(:anime_search_index, [:document], using: :gin)
  end

  def down do
    execute "DROP MATERIALIZED VIEW theme_search_index"
    execute "DROP MATERIALIZED VIEW anime_search_index"
  end
end
