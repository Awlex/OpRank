defmodule OpRank.Repo.Migrations.CreateScores do
  use Ecto.Migration

  def change do
    create table(:scores, primary_key: false) do
      add :wins, :integer
      add :loses, :integer
      add :score, :float

      add :theme_id, references(:themes, on_delete: :delete_all, on_update: :update_all, type: :uuid), primary_key: true
    end

    create index(:scores, [:wins])
    create index(:scores, [:loses])
    create index(:scores, [:score])
  end
end
