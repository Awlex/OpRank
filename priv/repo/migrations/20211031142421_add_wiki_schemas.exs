defmodule OpRank.Repo.Migrations.AddWikiSchemas do
  use Ecto.Migration

  def change do
    create table(:anime, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string, null: false

      timestamps(type: :utc_datetime)
    end

    create table(:themes, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :type, :string, null: false
      add :index, :integer, null: false

      add :anime_id,
          references(:anime, type: :binary_id, on_delete: :delete_all, on_update: :update_all)

      timestamps(type: :utc_datetime)
    end

    create table(:versions, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :version, :integer

      add :theme_id,
          references(:themes, type: :binary_id, on_delete: :delete_all, on_update: :update_all)

      timestamps(type: :utc_datetime)
    end

    create table(:urls, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :url, :string, null: false
      add :resolution, :integer, default: 720

      add :version_id,
          references(:versions, type: :binary_id, on_delete: :delete_all, on_update: :update_all)

      timestamps(type: :utc_datetime)
    end

    create unique_index(:anime, [:name])
    create unique_index(:themes, [:anime_id, :type, :index])
    create unique_index(:versions, [:theme_id, :version])
    create unique_index(:urls, [:version_id, :url])
  end
end
