module.exports = {
  mode: 'jit',
  content: [
    "./js/**/*.js",
    "../lib/*_web/**/*.*ex"
  ],
  theme: {
    extend: {},
  },
  plugins: [
    require('daisyui')
  ],
}
