defmodule OpRank.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      OpRank.Repo,
      OpRankWeb.Telemetry,
      {Phoenix.PubSub, name: OpRank.PubSub},
      OpRankWeb.Endpoint,
      OpRank.Scheduler,
      OpRank.Cache
    ]

    opts = [strategy: :one_for_one, name: OpRank.Supervisor]
    Supervisor.start_link(children, opts)
  end

  @impl true
  def config_change(changed, _new, removed) do
    OpRankWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
