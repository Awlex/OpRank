defmodule OpRank.Scoreboard do
  alias OpRank.Repo
  alias OpRank.Voting.Vote
  alias OpRank.Scoreboard.Score
  import Ecto.Query

  def scoreboard(opts \\ []) do
    paginate_params = Keyword.get(opts, :pagination, %{page: 1, page_size: 10})
    sorting = Keyword.get(opts, :sorting)

    Score
    |> preload(theme: :anime)
    |> select([s], %{
      s
      | position: over(row_number(), order_by: [desc: s.score])
    })
    |> order_by(^sorting)
    |> Repo.paginate(paginate_params)
  end

  def update_scoreboard() do
    wins =
      Vote
      |> select([v], {v.winner, count(v.winner)})
      |> group_by(:winner)
      |> Repo.all()
      |> Map.new(fn {k, v} -> {k, {v, 0}} end)

    loses =
      Vote
      |> select([v], {v.loser, count(v.loser)})
      |> group_by(:loser)
      |> Repo.all()
      |> Map.new(fn {k, v} -> {k, {0, v}} end)

    scores =
      Map.merge(wins, loses, fn _, {wins, _}, {_, loses} ->
        {wins, loses}
      end)
      |> Enum.map(fn {k, {wins, loses}} ->
        count = wins + loses
        score = 5 * ((wins - loses) / count + 1)
        %{theme_id: k, wins: wins, loses: loses, score: score}
      end)

    Repo.insert_all(Score, scores, on_conflict: :replace_all, conflict_target: :theme_id)
  end
end
