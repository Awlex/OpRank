defmodule OpRank.AnimeThemes do
  alias OpRank.{Cache, Repo}
  alias OpRank.AnimeThemes.{Anime, Theme, Url, Version}
  alias Ecto.Multi

  import Ecto.Query
  import OpRank.FullTextSearchHelpers

  def create_anime!(data) do
    %Anime{}
    |> Anime.changeset(data)
    |> Repo.insert!(
      on_conflict: {:replace_all_except, [:name]},
      conflict_target: [:name],
      returning: true
    )
  end

  def create_theme!(anime_id, data) do
    %Theme{anime_id: anime_id}
    |> Theme.changeset(data)
    |> Repo.insert!(
      on_conflict: {:replace_all_except, [:id, :anime_id, :type, :index]},
      conflict_target: [:anime_id, :type, :index],
      returning: true
    )
  end

  def create_version!(theme_id, data) do
    %Version{theme_id: theme_id}
    |> Version.changeset(data)
    |> Repo.insert!(
      on_conflict: {:replace_all_except, [:id, :theme_id, :version]},
      conflict_target: [:theme_id, :version],
      returning: true
    )
  end

  def create_url!(version_id, data) do
    %Url{version_id: version_id}
    |> Url.changeset(data)
    |> Repo.insert!(
      on_conflict: {:replace_all_except, [:id, :version_id, :url]},
      conflict_target: [:version_id, :url],
      returning: true
    )
  end

  def find_theme(theme_id, opts \\ []) do
    Repo.find(Theme, theme_id, opts)
  end

  def search_suggestions(query) do
    %{
      themes: theme_suggestions(query),
      anime: anime_suggestions(query)
    }
  end

  defp theme_suggestions(query) do
    "theme_search_index"
    |> where([d], ts_match(d.document, plainto_tsquery(^query)))
    |> order_by([d], desc: [ts_rank(d.document, plainto_tsquery(^query))])
    |> select([d], %{
      id: d.id,
      theme_name: d.theme_name,
      type: d.type,
      index: d.index,
      anime_name: d.anime_name
    })
    |> limit(10)
    |> Repo.all()
  end

  defp anime_suggestions(query) do
    "anime_search_index"
    |> where([d], ts_match(d.document, plainto_tsquery(^query)))
    |> order_by([d], desc: [ts_rank(d.document, plainto_tsquery(^query))])
    |> join(:inner, [d], a in Anime, on: d.id == a.id)
    |> select([_, a], a)
    |> limit(10)
    |> Repo.all()
    |> Repo.preload(:themes)
  end

  def generate_seed(), do: :rand.uniform()
  def apply_seed(seed), do: :rand.seed(:default, round(seed * 1_000_000))

  @type compare_type :: :op | :ed | :mixed
  @type vote_opt :: {:seed, integer} | {:compare_type, compare_type()}
  @spec themes_for_voting([vote_opt()]) :: {Theme.t(), Theme.t()}
  def themes_for_voting(opts \\ []) do
    seed = Keyword.get_lazy(opts, :seed, &generate_seed/0)
    compare_type = Keyword.get(opts, :compare_type, :op)
    min_year = Keyword.get(opts, :min_year)
    max_year = Keyword.get(opts, :max_year)
    seasons = get_seasons(opts)

    Cache.get_or_store({seed, compare_type, min_year, max_year, seasons}, fn ->
      apply_seed(seed)

      multi =
        Multi.new()
        |> Multi.run(:seed, fn repo, _ ->
          repo.query("select setseed($1)", [seed])
        end)
        |> Multi.run(:first_theme, fn repo, _ ->
          {:ok,
           Theme
           |> join(:inner, [t], assoc(t, :anime))
           |> apply_compare_type(compare_type)
           |> apply_year(min_year, max_year)
           |> apply_seasons(seasons)
           |> preload([_, a], [:score, versions: :urls, anime: a])
           |> order_by(fragment("random()"))
           |> limit(1)
           |> repo.one!()}
        end)
        |> Multi.run(:second_theme, fn repo, %{first_theme: theme} ->
          base =
            Theme
            |> join(:inner, [t], assoc(t, :anime), as: :anime)
            |> apply_compare_type(compare_type)
            |> apply_year(min_year, max_year)
            |> apply_seasons(seasons)
            |> preload([anime: a], [:score, anime: a, versions: :urls])
            |> limit(1)

          query =
            if theme.score != nil and :rand.uniform() > 0.5 do
              base
              |> join(:left, [t], assoc(t, :score), as: :score)
              |> order_by([score: s], [
                {:asc, fragment("ABS(?)", coalesce(s.score, 0) - ^theme.score.score)},
                fragment("random()")
              ])
            else
              order_by(base, fragment("RANDOM()"))
            end

          {:ok, repo.one(query)}
        end)

      {:ok, %{first_theme: a, second_theme: b}} = Repo.transaction(multi)

      {a, b}
    end)
  end

  def year_range() do
    Anime
    |> select([a], {min(a.year), max(a.year)})
    |> Repo.one!()
  end

  defp apply_compare_type(query, :mixed), do: query
  defp apply_compare_type(query, type), do: where(query, type: ^type)

  defp apply_year(query, nil, nil), do: query
  defp apply_year(query, year, year), do: where(query, [_, a], a.year == ^year)

  defp apply_year(query, min, max) when is_integer(min),
    do: where(query, [_, a], ^min <= a.year) |> apply_year(nil, max)

  defp apply_year(query, _, max) when is_integer(max), do: where(query, [_, a], a.year <= ^max)

  defp apply_seasons(query, nil), do: query
  defp apply_seasons(query, []), do: query

  defp apply_seasons(query, seasons) do
    where(query, [_, a], a.season in ^seasons)
  end

  defp get_seasons(opts) do
    seasons =
      case Keyword.get(opts, :seasons) do
        seasons when is_map(seasons) -> for {season, true} <- seasons, do: season
        other -> other
      end

    unless seasons == [], do: seasons
  end
end
