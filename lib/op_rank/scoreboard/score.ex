defmodule OpRank.Scoreboard.Score do
  use Ecto.Schema

  @primary_key false
  schema "scores" do
    field :wins, :integer
    field :loses, :integer
    field :score, :float
    field :position, :integer, virtual: true

    belongs_to :theme, OpRank.AnimeThemes.Theme, type: :binary_id
  end
end
