defmodule OpRank.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "users" do
    field :uid, :string
    field :provider, :string
    field :name, :string
    field :avatar_url, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:uid, :provider, :name, :avatar_url])
    |> validate_required([:uid, :provider, :name, :avatar_url])
    |> unique_constraint([:uid, :provider])
  end
end
