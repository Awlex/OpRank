defmodule OpRank.Accounts.Settings do
  use Ecto.Schema
  alias OpRank.Accounts.User
  import Ecto.Changeset

  @primary_key false
  schema "account_settings" do
    field :key, :string, primary_key: true
    field :value, :any, virtual: true
    field :encoded, :string, source: :value

    belongs_to :user, User, primary_key: true, type: :binary_id
  end

  def changeset(setting, params) do
    setting
    |> cast(params, [:key, :value, :user_id])
    |> validate_required([:key, :value, :user_id])
    |> assoc_constraint(:user)
    |> encode_change()
  end

  def encode(term), do: :erlang.term_to_binary(term)
  def decode(term), do: :erlang.binary_to_term(term)

  defp encode_change(changeset) do
    cond do
      not changeset.valid? ->
        changeset

      value = get_change(changeset, :value) ->
        put_change(changeset, :encoded, encode(value))

      true ->
        changeset
    end
  end
end
