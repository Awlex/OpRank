defmodule OpRank.FullTextSearchHelpers do
  defmacro plainto_tsquery(query) do
    quote do
      fragment("plainto_tsquery(?)", unquote(query))
    end
  end

  defmacro plainto_tsquery(language, query) do
    quote do
      fragment("plainto_tsquery(?, ?)", unquote(language), unquote(query))
    end
  end

  defmacro ts_rank(document, query) do
    quote do
      fragment("ts_rank(?, ?)", unquote(document), unquote(query))
    end
  end

  defmacro ts_match(document, query) do
    quote do
      fragment("? @@ ?", unquote(document), unquote(query))
    end
  end
end
