defmodule OpRank.Voting do
  alias OpRank.Repo
  alias OpRank.Voting.Vote

  def save_vote(user, winner, loser) do
    data = %{user_id: user.id, winner: winner, loser: loser}

    %Vote{}
    |> Vote.changeset(data)
    |> Repo.insert(
      on_conflict: :replace_all,
      conflict_target:
        {:unsafe_fragment, "(user_id, least(winner, loser), greatest(winner, loser))"}
    )
  end
end
