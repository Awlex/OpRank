defmodule OpRank.Pagination do
  defmodule Page do
    defstruct [:entries, :page, :page_size, :total_pages, :total_entries]

    defimpl Enumerable do
      def count(page), do: Enumerable.count(page.entries)
      def member?(page, element), do: Enumerable.member?(page.entries, element)
      def reduce(page, acc, fun), do: Enumerable.reduce(page.entries, acc, fun)
      def slice(page), do: Enumerable.slice(page.entries)
    end
  end

  defmodule Opts do
    @type type :: :offset_limit | :row_number
    defstruct page: 1, page_size: 10, type: :offset_limit
  end

  @spec pagination_opts(any) :: %OpRank.Pagination.Opts{
          page: non_neg_integer(),
          page_size: non_neg_integer(),
          type: Opts.type()
        }
  def pagination_opts(opts) do
    opts = Map.new(opts || [], fn {k, v} -> {to_string(k), v} end)

    %Opts{
      page: parse_page(opts["page"]),
      page_size: parse_page_size(opts["page_size"]),
      type: parse_type(opts["type"])
    }
  end

  defp parse_int(unsafe) when is_binary(unsafe) do
    case Integer.parse(unsafe) do
      {int, _} -> int
      :error -> -1
    end
  end

  defp parse_int(unsafe) when is_integer(unsafe), do: unsafe
  defp parse_int(_), do: -1

  defp parse_page(page), do: max(parse_int(page), 1)
  defp parse_page_size(page_size), do: max(parse_int(page_size), 10)

  defp parse_type(type) when type in ~w"offset_limit row_number"a, do: type
  defp parse_type(_), do: :offset_limit
end
