defmodule OpRank.Voting.Vote do
  use Ecto.Schema
  import Ecto.Changeset

  alias OpRank.AnimeThemes.Theme
  alias OpRank.Accounts.User

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "votes" do
    belongs_to :winner_theme, Theme, foreign_key: :winner
    belongs_to :loser_theme, Theme, foreign_key: :loser
    belongs_to :user, User

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(vote, attrs) do
    vote
    |> cast(attrs, [:user_id, :winner, :loser])
    |> validate_required([:user_id, :winner, :loser])
    |> check_constraint(:winner, name: :different_votes)
  end
end
