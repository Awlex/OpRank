defmodule OpRank.Accounts do
  alias OpRank.Repo
  alias OpRank.Accounts.{Settings, User}
  import Ecto.Query

  def find_user(id), do: Repo.find(User, id)

  def sign_in(data) do
    with {:error, :not_found} <- Repo.find(User, provider: data.provider, uid: data.uid) do
      %User{}
      |> User.changeset(data)
      |> Repo.insert()
    end
  end

  @type user_id :: binary
  @type settings :: %{atom => term}

  @spec put_setting(user_id(), atom | String.t(), any) ::
          {:ok, %Settings{}} | {:error, Ecto.Changeset.t()}
  def put_setting(user_id, key, value) do
    %Settings{}
    |> Settings.changeset(%{user_id: user_id, key: to_string(key), value: value})
    |> Repo.insert(on_conflict: {:replace, [:encoded]}, conflict_target: [:user_id, :key])
  end

  @spec put_settings(user_id, settings()) :: :ok
  def put_settings(user_id, settings) do
    settings =
      Enum.map(settings, fn {key, value} ->
        %{user_id: user_id, key: to_string(key), encoded: Settings.encode(value)}
      end)

    Repo.insert_all(Settings, settings,
      on_conflict: {:replace, [:encoded]},
      conflict_target: [:user_id, :key]
    )

    :ok
  end

  @spec get_setting(any, any) :: {:error, :not_found} | {:ok, term}
  def get_setting(user_id, key) do
    with {:ok, %Settings{encoded: encoded}} <-
           Repo.find(Settings, user_id: user_id, key: to_string(key)) do
      {:ok, :erlang.binary_to_term(encoded)}
    end
  end

  @spec get_setting(user_id, atom | String.t(), term) :: {:error, :not_found} | {:ok, term}
  def get_setting(user_id, key, default) do
    case Repo.find(Settings, user_id: user_id, key: to_string(key)) do
      {:ok, %Settings{encoded: encoded}} -> :erlang.binary_to_term(encoded)
      {:error, :not_found} -> default
    end
  end

  @spec restore_settings(binary, %{atom => term}) :: %{atom => term}
  def restore_settings(user_id, defaults) do
    restore_settings(user_id, Map.keys(defaults), defaults)
  end

  @spec restore_settings(binary, [atom], %{atom => term}) :: %{atom => term}
  def restore_settings(user_id, keys, defaults) do
    keys = Enum.map(keys, &to_string/1)

    Settings
    |> where([s], s.user_id == ^user_id and s.key in ^keys)
    |> select([s], {s.key, s.encoded})
    |> Repo.all()
    |> Enum.reduce(defaults, fn {k, v}, acc ->
      Map.put(acc, String.to_atom(k), Settings.decode(v))
    end)
  end
end
