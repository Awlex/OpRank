defmodule OpRank.Repo do
  use Ecto.Repo,
    otp_app: :op_rank,
    adapter: Ecto.Adapters.Postgres

  import Ecto.Query

  alias OpRank.Pagination

  def find(queryable, field, opts \\ [])

  def find(queryable, fields, opts) when is_list(fields) do
    queryable
    |> get_by(fields)
    |> handle_find_result(opts)
  end

  def find(queryable, id, opts) do
    queryable
    |> get(id)
    |> handle_find_result(opts)
  end

  defp handle_find_result(nil, _) do
    {:error, :not_found}
  end

  defp handle_find_result(value, opts) do
    {:ok, __MODULE__.preload(value, Keyword.get(opts, :preload, []))}
  end

  def paginate(query, pagination_opts) do
    total_entries =
      query
      |> exclude(:order_by)
      |> exclude(:limit)
      |> exclude(:offset)
      |> exclude(:select)
      |> exclude(:preload)
      |> select([e], count(e))
      |> one()

    %Pagination.Opts{page: page, page_size: page_size, type: type} =
      Pagination.pagination_opts(pagination_opts)

    total_pages = ceil(total_entries / page_size)
    offset = (page - 1) * page_size

    entries =
      case type do
        :offset_limit ->
          query
          |> offset(^offset)
          |> limit(^page_size)
          |> all()

        :row_number ->
          query
          |> subquery()
          |> where([e], e.row_number >= ^offset)
          |> limit(^page_size)
          |> all()
      end

    %Pagination.Page{
      entries: entries,
      total_pages: total_pages,
      total_entries: total_entries,
      page: page,
      page_size: page_size
    }
  end
end
