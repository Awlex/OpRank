defmodule OpRank.AnimeThemes.Theme do
  use Ecto.Schema
  import Ecto.Changeset

  alias OpRank.AnimeThemes.{Anime, Version}

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "themes" do
    field :name, :string
    field :type, Ecto.Enum, values: [:op, :ed]
    field :index, :integer

    belongs_to :anime, Anime, type: :binary_id
    has_many :versions, Version
    has_one :score, OpRank.Scoreboard.Score

    timestamps type: :utc_datetime
  end

  def changeset(struct, data \\ %{}) do
    struct
    |> cast(data, [:anime_id, :name, :type, :index])
    |> validate_required([:anime_id, :type, :index])
    |> unique_constraint([:anime_id, :type, :index])
    |> assoc_constraint(:anime)
  end
end
