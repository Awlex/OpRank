defmodule OpRank.AnimeThemes.Anime.Details do
  use Ecto.Schema
  import Ecto.Changeset

  schema "anime_details" do
    field :season, Ecto.Enum, values: [:winter, :spring, :summer, :fall]
    field :year, :integer, null: false
    field :cover_image_medium, :string
    field :cover_image_large, :string

    belongs_to :anime, OpRank.AnimeThemes.Anime, type: :binary_id

    timestamps(type: :utc_datetime)
  end

  def changeset(struct, data) do
    struct
    |> cast(data, [
      :anime_id,
      :season,
      :year,
      :cover_image_medium,
      :cover_image_large
    ])
    |> validate_required([
      :anime_id,
      :season,
      :year,
      :cover_image_medium,
      :cover_image_large
    ])
    |> assoc_constraint(:anime)
  end
end
