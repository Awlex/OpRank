defmodule OpRank.AnimeThemes.Url do
  use Ecto.Schema
  import Ecto.Changeset

  alias OpRank.AnimeThemes.Version

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "urls" do
    field :name, :string
    field :url, :string
    field :resolution, :integer, default: 720

    belongs_to :version, Version, type: :binary_id

    timestamps type: :utc_datetime
  end

  def changeset(struct, data \\ %{}) do
    struct
    |> cast(data, [:version_id, :url, :resolution])
    |> validate_required([:version_id, :url, :resolution])
    |> unique_constraint([:version_id, :url])
    |> assoc_constraint(:version)
  end
end
