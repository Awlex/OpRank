defmodule OpRank.AnimeThemes.Anime do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "anime" do
    field :name, :string
    field :mal_id, :integer
    field :year, :integer
    field :season, Ecto.Enum, values: [:fall, :spring, :summer, :winter]

    has_one :details, OpRank.AnimeThemes.Anime.Details
    has_many :themes, OpRank.AnimeThemes.Theme

    timestamps type: :utc_datetime
  end

  def changeset(struct, data \\ %{}) do
    struct
    |> cast(data, [:name, :mal_id, :year, :season])
    |> validate_required([:name])
    |> unique_constraint(:name)
    |> unique_constraint(:mal_id)
  end

  def details_changeset(struct, params) do
    cast(struct, params, [:al_id])
  end
end
