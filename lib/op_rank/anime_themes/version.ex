defmodule OpRank.AnimeThemes.Version do
  use Ecto.Schema
  import Ecto.Changeset

  alias OpRank.AnimeThemes.{Theme, Url}

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "versions" do
    field :version, :integer

    belongs_to :theme, Theme, type: :binary_id
    has_many :urls, Url

    timestamps type: :utc_datetime
  end

  def changeset(struct, data \\ %{}) do
    struct
    |> cast(data, [:theme_id, :version])
    |> validate_required([:theme_id, :version])
    |> unique_constraint([:theme_id, :version])
    |> assoc_constraint(:theme)
  end
end
