defmodule OpRank.Cache do
  def child_spec(_) do
    Supervisor.child_spec(
      {ConCache,
       name: __MODULE__,
       global_ttl: :timer.minutes(1),
       touch_on_read: true,
       ttl_check_interval: :timer.seconds(30)},
      []
    )
  end

  def get_or_store(key, default_fun) do
    ConCache.get_or_store(__MODULE__, key, default_fun)
  end
end
