defmodule OpRank.Wiki.Importer do
  alias OpRank.{AnimeThemes, Repo}
  alias OpRank.Wiki.{Client, Parser}

  def import_from_wiki() do
    Client.fetch_year_index!()
    |> get_in(~w"data content_md")
    |> Parser.parse_years()
    |> Task.async_stream(&Client.fetch_year!/1, timeout: 60_000)
    |> Stream.map(fn {:ok, result} -> get_in(result, ~w"data content_md") end)
    |> Stream.flat_map(&Parser.parse_year/1)
    |> Enum.each(&upsert_anime!/1)

    Repo.query!("REFRESH MATERIALIZED VIEW theme_search_index")
    Repo.query!("REFRESH MATERIALIZED VIEW anime_search_index")
  end

  defp upsert_anime!(data) do
    anime = AnimeThemes.create_anime!(data)
    Enum.each(data.themes, &upsert_theme!(anime.id, &1))
  end

  defp upsert_theme!(anime_id, data) do
    theme = AnimeThemes.create_theme!(anime_id, data)
    Enum.each(data.versions, &upsert_version!(theme.id, &1))
  end

  defp upsert_version!(theme_id, data) do
    version = AnimeThemes.create_version!(theme_id, data)
    Enum.each(data.urls, &AnimeThemes.create_url!(version.id, &1))
  end
end
