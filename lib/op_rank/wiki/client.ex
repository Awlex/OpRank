defmodule OpRank.Wiki.Client do
  use Tesla, only: [:get]

  plug Tesla.Middleware.BaseUrl, "https://old.reddit.com"
  plug Tesla.Middleware.JSON

  def fetch_year_index!() do
    fetch!("r/AnimeThemes/wiki/year_index.json")
  end

  def fetch_year!(path) do
    fetch!(path <> ".json")
  end

  defp fetch!(path) do
    cache = Application.get_env(:op_rank, __MODULE__)[:cache] || false
    cache_file = Path.join([System.tmp_dir!(), "op_rank", path])

    if cache and File.exists?(cache_file) do
      cache_file
      |> File.read!()
      |> :erlang.binary_to_term()
    else
      {:ok, %Tesla.Env{status: 200, body: body}} = get(path)

      if cache do
        Path.dirname(cache_file) |> File.mkdir_p!()
        File.write!(cache_file, :erlang.term_to_binary(body))
      end

      body
    end
  end
end
