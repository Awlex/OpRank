defmodule OpRank.Wiki.Parser do
  def parse_years(years) do
    Regex.scan(~r/^###\s*\[[^\]]+\]\(([^\)]+)\)/m, years)
    |> Enum.map(&List.last/1)
  end

  def parse_year(year) do
    year
    |> String.split("\n")
    |> Enum.reduce({[], {nil, nil}}, &parse_anime/2)
    |> elem(0)
    |> finalize_last()
    |> Enum.reverse()
  end

  # This is not overengeneered
  @theme_regex ~r/((?<type>OP|ED)(?<index>\d+)?(\s+V(?<version>\d+))?\s+("(?<name>[^"]+)")?(?<artist>[^|]+)?)?\|\[(?<urlname>[^\]]+)\]\((?<url>[^)]+)/
  @title_regex ~r/###\s*\[(?<name>.+)\](\(https?:\/\/myanimelist.net\/anime\/(?<mal_id>\d+)\/?[^)]*\))?/
  @season_regex ~r/##(?<year>\d+) (?<season>Fall|Summer|Spring|Winter)/i

  defp parse_anime(line, {acc, {year, season} = time}) do
    cond do
      match = Regex.named_captures(@season_regex, line) ->
        season = String.to_atom(String.downcase(match["season"]))
        year = String.to_integer(match["year"])
        {acc, {year, season}}

      match = Regex.named_captures(@title_regex, line) ->
        name = match["name"]
        mal_id = if match["mal_id"] not in ["", nil], do: String.to_integer(match["mal_id"])
        new_anime = %{name: name, mal_id: mal_id, themes: [], year: year, season: season}
        {[new_anime | finalize_last(acc)], time}

      match = Regex.named_captures(@theme_regex, line, capture: :all_names) ->
        [current | rest] = acc

        new_current =
          if match["type"] == "" do
            [last | rest] = current.themes
            %{current | themes: [add_url(last, match) | rest]}
          else
            %{current | themes: add_theme(current.themes, match)}
          end

        {[new_current | rest], time}

      true ->
        {acc, time}
    end
  end

  defp finalize_last([]), do: []

  defp finalize_last([theme | rest]) do
    new_theme =
      Map.update!(theme, :themes, fn themes ->
        Enum.map(themes, fn theme ->
          Map.update!(theme, :versions, fn versions ->
            versions
            |> Enum.map(fn version -> Map.update!(version, :urls, &Enum.reverse/1) end)
            |> Enum.sort_by(& &1.version, :asc)
          end)
        end)
        |> Enum.sort_by(&{&1.type, &1.index})
      end)

    [new_theme | rest]
  end

  defp add_theme([], theme), do: [parse_theme(theme)]

  defp add_theme([last | rest] = themes, theme) do
    parsed = parse_theme(theme)

    if last.index == parsed.index and last.type == parsed.type do
      new_last = Map.update!(last, :versions, &[parse_version(theme) | &1])
      [new_last | rest]
    else
      [parse_theme(theme) | themes]
    end
  end

  @type_map %{"OP" => :op, "ED" => :ed}
  defp parse_theme(theme) do
    %{
      name: Map.get(theme, "name", ""),
      type: @type_map[Map.fetch!(theme, "type")],
      index: to_integer(theme["index"], 1),
      versions: [parse_version(theme)]
    }
  end

  defp parse_version(theme) do
    %{
      version: to_integer(theme["version"], 1),
      urls: [
        %{
          name: Map.get(theme, "urlname", ""),
          url: Map.fetch!(theme, "url"),
          resolution: parse_resolution(Map.get(theme, "urlname"))
        }
      ]
    }
  end

  defp add_url(current_theme, new_theme) do
    Map.update!(current_theme, :versions, fn
      [] ->
        []

      [current | rest] ->
        new_current =
          Map.update!(current, :urls, fn urls ->
            new_url = %{
              name: Map.get(new_theme, "urlname", ""),
              url: Map.fetch!(new_theme, "url")
            }

            [new_url | urls]
          end)

        [new_current | rest]
    end)
  end

  defp parse_resolution(nil), do: 720

  defp parse_resolution(name) do
    case Regex.run(~r/\d+/, name) do
      [resolution] -> String.to_integer(resolution)
      _ -> 720
    end
  end

  defp to_integer(text, default) do
    case Integer.parse(text) do
      {int, _} when is_integer(int) -> int
      _ -> default
    end
  end
end
