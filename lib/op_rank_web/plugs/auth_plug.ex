defmodule OpRankWeb.Plugs.AuthPlug do
  alias OpRankWeb.Router.Helpers, as: Routes

  alias OpRank.Accounts
  import Plug.Conn
  import Phoenix.Controller, only: [redirect: 2, put_flash: 3]

  def load_user(conn, _) do
    with user_id when is_binary(user_id) <- get_session(conn, :user_id),
         {:ok, user} <- Accounts.find_user(user_id) do
      assign(conn, :current_user, user)
    else
      _ ->
        conn
    end
  end

  def ensure_user(conn, _) do
    if conn.assigns[:current_user] do
      conn
    else
      conn
      |> put_flash(:error, "You need to sign in before you can continue")
      |> put_resp_cookie("redirect", conn.request_path)
      |> redirect(to: Routes.auth_path(conn, :index))
      |> halt()
    end
  end

  def ensure_no_user(conn, _) do
    if !conn.assigns[:current_user] do
      conn
    else
      conn
      |> put_flash(:error, "You are already signed in")
      |> redirect(to: Routes.page_path(conn, :index))
      |> halt()
    end
  end
end
