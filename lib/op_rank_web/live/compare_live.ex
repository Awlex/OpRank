defmodule OpRankWeb.CompareLive do
  use OpRankWeb, :live_view

  alias OpRank.{Accounts, AnimeThemes, Voting}

  import Ecto.Changeset

  on_mount {__MODULE__, :years}
  on_mount {__MODULE__, :restore_settings}

  def mount(%{"a" => a, "b" => b}, _session, socket) do
    socket =
      with {:ok, a} <- AnimeThemes.find_theme(a, preload: [:anime, :score, versions: :urls]),
           {:ok, b} <- AnimeThemes.find_theme(b, preload: [:anime, :score, versions: :urls]) do
        assign(socket, a: a, b: b, random: false, chosen: false, chosen_theme: nil)
      else
        _ ->
          socket
          |> put_flash(:error, "Theme not found")
          |> redirect(Routes.compare_path(socket, :random))
      end

    {:ok, socket}
  end

  def mount(_params, _session, socket) do
    {a, b} = AnimeThemes.themes_for_voting(Keyword.new(socket.assigns.settings))
    assigns = %{a: a, b: b, random: true, chosen: false, chosen_theme: nil}

    {:ok, assign(socket, assigns)}
  end

  def handle_event("update_settings", %{"data" => data}, socket) do
    changeset =
      cast({socket.assigns.settings, compare_types()}, data, [:seasons, :min_year, :max_year])

    min_year = get_field(changeset, :min_year)
    max_year = get_field(changeset, :max_year)

    changeset =
      if min_year > max_year do
        add_error(changeset, :min_year, "May not be greater than max year")
      else
        changeset
      end

    if changeset.valid? do
      settings = apply_changes(changeset)

      # Update user settings if necessary
      if current_user = socket.assigns[:current_user],
        do: Accounts.put_settings(current_user.id, settings)

      {:noreply, assign(socket, changeset: changeset, settings: settings)}
    else
      {:noreply, socket}
    end
  end

  def handle_event("update_seasons", %{"season" => season}, socket) do
    season = String.to_existing_atom(season)
    settings = socket.assigns.settings

    seasons =
      if season in settings.seasons do
        settings.seasons -- [season]
      else
        [season | settings.seasons]
      end

    settings = Map.put(settings, :seasons, seasons)

    if current_user = socket.assigns[:current_user],
      do: Accounts.put_setting(current_user.id, :seasons, seasons)

    {:noreply, assign(socket, settings: settings)}
  end

  def handle_event("set_winner", %{"winner" => winner}, socket) do
    %{a: a, b: b, current_user: current_user} = socket.assigns

    {winner, loser} =
      if a.id == winner do
        {a.id, b.id}
      else
        {b.id, a.id}
      end

    {:ok, _} = Voting.save_vote(current_user, winner, loser)
    {:noreply, assign(socket, chosen: true, chosen_theme: winner)}
  end

  def handle_event("next", _, socket) do
    {:noreply, regenerate_seed(socket)}
  end

  ## Components

  defp video_player(assigns) do
    ~H"""
    <div class={"flex justify-between #{status_class(@chosen, @theme.id, @chosen_theme)}"}>
      <div>
        <h2 class="text-xl font-bold">
            <span class="uppercase"><%= @theme.type %><%= @theme.index %></span> <%= surround(@theme.name) %>
        </h2>
        <h3 class="text-md">From <span class="font-semibold"><%= @theme.anime.name %></span></h3>
      </div>
      <%= if @chosen do %>
        <div class="p-2 bg-base-200 rounded-md w-24">
          <p><b>Score:</b></p>
          <p>
            <%= if @theme.score do %>
              <%= round @theme.score.score * 10 %> / 100
            <% else %>
              Unranked
            <% end %>
          </p>
        </div>
      <% end %>
    </div>

    <div class="mt-5">
        <video id={@theme.id} src={video_url(@theme)} controls preload="none" class="w-full bg-black"/>
    </div>
    """
  end

  defp choose_form(assigns) do
    ~H"""
    <div data-tip="Please sign in first" class={"mt-2 #{if @chosen, do: "hidden" } #{if !assigns[:current_user],do: "tooltip tooltip-bottom tooltip-warning btn-block"}"}>
      <button class={"btn btn-primary btn-block #{!assigns[:current_user] && "btn-disabled"}"}
              phx-click="set_winner"
              phx-value-winner={@winner}>
        Choose
      </button>
    </div>
    """
  end

  defp video_url(theme) do
    version = hd(theme.versions)

    url =
      Enum.find(version.urls, &(&1.resolution == 720)) ||
        Enum.find(version.urls, &(&1.resolution == 1080)) ||
        Enum.max_by(version.urls, & &1.resolution)

    url.url
  end

  defp compare_types() do
    %{
      seasons: {:array, {:parameterized, Ecto.Enum, Ecto.Enum.init(values: seasons())}},
      min_year: :integer,
      max_year: :integer
    }
  end

  defp seasons() do
    Ecto.Enum.values(AnimeThemes.Anime, :season)
  end

  # mount hooks

  def on_mount(:years, _, _, socket) do
    {min_year, max_year} = AnimeThemes.year_range()
    {:cont, assign(socket, min_year: min_year, max_year: max_year)}
  end

  def on_mount(:restore_settings, _, _, socket) do
    defaults = %{
      compare_type: :op,
      min_year: socket.assigns.min_year || 2000,
      max_year: socket.assigns.max_year || 2021,
      seed: AnimeThemes.generate_seed(),
      seasons: []
    }

    settings =
      if current_user = socket.assigns[:current_user] do
        Accounts.restore_settings(current_user.id, defaults)
      else
        defaults
      end

    changeset = Ecto.Changeset.change({settings, compare_types()})

    {:cont, assign(socket, changeset: changeset, settings: settings)}
  end

  defp season_map(seasons), do: Map.new(seasons(), &{&1, &1 in seasons})

  defp regenerate_seed(socket) do
    settings = socket.assigns.settings
    seed = AnimeThemes.generate_seed()

    if user = socket.assigns[:current_user] do
      {:ok, _} = Accounts.put_setting(user.id, :seed, seed)
    end

    settings = Map.put(settings, :seed, seed)
    {a, b} = AnimeThemes.themes_for_voting(Keyword.new(settings))

    assign(socket, %{settings: settings, a: a, b: b, chosen: false, chosen_theme: nil})
  end

  defp status_class(false, _, _), do: ""
  defp status_class(true, id, id), do: "text-green-400"
  defp status_class(_, _, _), do: "text-red-400"
end
