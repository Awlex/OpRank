defmodule OpRankWeb.ScoreboardLive do
  use OpRankWeb, :live_view
  alias OpRank.Scoreboard

  on_mount OpRankWeb.PaginationHelpers

  def mount(_params, _session, socket) do
    {:ok, update_scoreboard(socket)}
  end

  defp update_scoreboard(socket) do
    pagination = Map.take(socket.assigns, [:page, :page_size])
    scores = Scoreboard.scoreboard(pagination: pagination)
    assign(socket, :scores, scores)
  end

  def handle_event("set_page", _, socket) do
    {:noreply, update_scoreboard(socket)}
  end
end
