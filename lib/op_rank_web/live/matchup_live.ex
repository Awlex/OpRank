defmodule OpRankWeb.MatchupLive do
  use OpRankWeb, :live_view

  alias OpRank.AnimeThemes

  defmodule Choice do
    defstruct query: "", suggestions: %{anime: [], themes: []}, selection: nil
  end

  def mount(_, _, socket) do
    assigns = %{chosen: false, a: %Choice{}, b: %Choice{}}

    {:ok, assign(socket, assigns)}
  end

  def search_form(assigns) do
    ~H"""
    <.form for={@for} let={f} phx-change="update_suggestions">
      <div class="bg-base-200 p-3">
        <input type="search" class="block bg-base-200 p-2 w-full mx-auto rounded-2 border-b outline-none" placeholder="Search for the theme" name={input_name(f, :query)} id={input_id(f, :query)} value={@selection.query} phx-debounce="500">
        <.anime_picker anime={@selection.suggestions.anime} for={@for}/>
        <.theme_picker themes={@selection.suggestions.themes} for={@for} />
      </div>
    </.form>
    """
  end

  def theme(assigns) do
    ~H"""
    <div class="p-3 rounded bg-base-200">
      <div class="flex justify-between">
        <div>
          <p class="text-xl">
            <span class="uppercase"><%= @theme.type %><%=@theme.index %></span>
            <b><%= @theme.name %></b>
          </p>
          <span class="text-sm">
            from <b><%= @theme.anime.name %></b>
          </span>
        </div>
        <span class="btn btn-sm w-10 h-10" phx-click="remove" phx-value-key={@key}>
          <.icon icon={:close} />
        </span>
      </div>

      <video id={@theme.id} src={video_url(@theme)} controls preload="none" class="w-full mt-2 bg-black"/>
    </div>
    """
  end

  defp anime_picker(assigns) do
    ~H"""
    <%= if Enum.any?(@anime) do %>
      <div class="p-2 my-3">
        <h3 class="text-3xl">Anime</h3>
        <div class="flex flex-col mt-2">
          <%= for anime <- @anime do %>
            <div phx-click={JS.toggle to: "#anime_collapse_#{anime.id}"}>
              <div class="flex justify-between items-center hover:bg-base-300 hover:cursor-pointer rounded-md">
                <span class="p-2 rounded hover:bg-base-300 hover:cursor-pointer text-xl">
                  <%= anime.name %>
                </span>
                <span class="cursor-pointer w-10 h-10" @click="">
                  <.icon icon={:caret} />
                </span>
              </div>
              <div id={"anime_collapse_#{anime.id}"} class="hidden ml-3">
                <%= for theme <- anime.themes do %>
                  <p class="p-2 rounded hover:bg-base-300 cursor-pointer" phx-click="select" phx-value-key={@for} phx-value-theme={theme.id}>
                    <span class="uppercase"><%= theme.type %><%= theme.index %></span>
                    <b><%= theme.name %></b>
                  </p>
                <% end %>
                <div class="divider"></div>
              </div>
            </div>
          <% end %>
        </div>
      </div>
    <% end %>
    """
  end

  defp theme_picker(assigns) do
    ~H"""
    <%= if Enum.any?(@themes) do %>
      <div class="p-2 my-3">
        <h3 class="text-3xl mb-2">Themes</h3>
        <div class="flex flex-col mt-2">
          <%= for theme <- @themes do %>
            <span class="p-2 rounded hover:bg-base-300 hover:cursor-pointer" phx-click="select" phx-value-key={@for} phx-value-theme={Ecto.UUID.load!(theme.id)}>
              <p class="text-xl">
                <span class="uppercase"> <%= theme.type %><%= theme.index %></span>
                <b><%= theme.theme_name %></b>
              </p>
              <span class="text-sm"> from <b><%= theme.anime_name %></b></span>
            </span>
          <% end %>
        </div>
      </div>
    <% end %>
    """
  end

  defp video_url(theme) do
    version = hd(theme.versions)

    url =
      Enum.find(version.urls, &(&1.resolution == 720)) ||
        Enum.find(version.urls, &(&1.resolution == 1080)) ||
        Enum.max_by(version.urls, & &1.resolution)

    url.url
  end

  def handle_event("update_suggestions", params, socket) do
    {key, query} =
      case params do
        %{"a" => %{"query" => query}} -> {:a, query}
        %{"b" => %{"query" => query}} -> {:b, query}
      end

    selection =
      if String.length(query) > 2 do
        %Choice{suggestions: AnimeThemes.search_suggestions(query), query: query}
      else
        %Choice{query: query}
      end

    {:noreply, assign(socket, key, selection)}
  end

  def handle_event("remove", %{"key" => key}, socket) do
    key = String.to_existing_atom(key)
    socket = update(socket, key, &%{&1 | selection: nil})
    socket = assign(socket, :chosen, false)
    {:noreply, socket}
  end

  def handle_event("select", %{"key" => key, "theme" => theme}, socket) do
    key = String.to_existing_atom(key)
    {:ok, theme} = AnimeThemes.find_theme(theme, preload: [:anime, :score, versions: :urls])
    socket = update(socket, key, fn selection -> %{selection | selection: theme} end)

    %{a: %{selection: theme_a}, b: %{selection: theme_b}} = socket.assigns
    chosen = theme_a != nil and theme_b != nil and theme_a.id != theme_b.id
    socket = assign(socket, :chosen, chosen)
    {:noreply, socket}
  end

  def handle_event("compare", _, socket) do
    %{a: %{selection: theme_a}, b: %{selection: theme_b}, chosen: chosen} = socket.assigns

    if chosen do
      {:noreply,
       push_redirect(socket, to: Routes.compare_path(socket, :matchup, theme_a, theme_b))}
    else
      {:noreply, put_flash(socket, :error, "Nice try, you need select 2 different themes!")}
    end
  end
end
