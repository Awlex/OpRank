defmodule OpRankWeb.MountHooks.Auth do
  alias OpRank.Accounts
  import Phoenix.LiveView

  def on_mount(:default, _, session, socket) do
    with {:ok, user_id} <- Map.fetch(session, "user_id"),
         {:ok, user} <- Accounts.find_user(user_id) do
      {:cont, assign(socket, :current_user, user)}
    else
      _ -> {:cont, socket}
    end
  end
end
