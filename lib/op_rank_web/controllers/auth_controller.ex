defmodule OpRankWeb.AuthController do
  use OpRankWeb, :controller

  alias OpRank.Accounts

  require Logger

  plug Ueberauth

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def delete(conn, _params) do
    conn
    |> put_flash(:info, "You have been logged out")
    |> clear_session()
    |> redirect(to: Routes.compare_path(conn, :random))
  end

  def callback(%{assigns: %{ueberauth_failure: failure}} = conn, _) do
    Logger.error(provider: failure.provider, error: failure.errors)

    conn
    |> put_flash(:error, "unable to sign in with #{failure.provider}")
    |> redirect(to: Routes.page_path(conn, :index))
  end

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, _) do
    redirect_path = conn.req_cookies["redirect"] || Routes.compare_path(conn, :random)

    data = %{
      uid: auth.uid,
      provider: to_string(auth.provider),
      name: auth.info.nickname,
      avatar_url: auth.info.image
    }

    case Accounts.sign_in(data) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Signed in with #{String.capitalize(to_string(auth.provider))}")
        |> delete_resp_cookie("redirect")
        |> put_session(:user_id, user.id)
        |> redirect(to: redirect_path)

      {:error, error} ->
        Logger.error(provider: auth.provider, error: error)

        conn
        |> put_flash(
          :error,
          "Unable to sign in with #{String.capitalize(to_string(auth.provider))}"
        )
        |> redirect(to: Routes.page_path(conn, :index))
    end
  end
end
