defmodule OpRankWeb.Router do
  use OpRankWeb, :router

  import OpRankWeb.Plugs.AuthPlug

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {OpRankWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :load_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :unauthorized do
    plug :ensure_no_user
  end

  pipeline :authorized do
    plug :ensure_user
  end

  scope "/", OpRankWeb do
    pipe_through :browser

    get "/about", PageController, :index

    live_session :default, on_mount: [OpRankWeb.MountHooks.Auth] do
      live "/compare", CompareLive, :random
      live "/compare/:a/:b", CompareLive, :matchup
      live "/scoreboard", ScoreboardLive, :index
      live "/matchup", MatchupLive, :index
      live "/", CompareLive, :random
    end
  end

  scope "/", OpRankWeb do
    pipe_through [:browser, :authorized]

    get "/auth/delete", AuthController, :delete
  end

  scope "/", OpRankWeb do
    pipe_through [:browser, :unauthorized]

    get "/auth/", AuthController, :index
    get "/auth/:provider", AuthController, :request
    get "/auth/:provider/callback", AuthController, :callback
  end

  scope "/", OpRankWeb do
    get "/config/getuser", TrollController, :show
    get "/phpmyadmin", TrollController, :show
    get "/boaform/admin/formLogin", TrollController, :show
    get "/dispatch.asp", TrollController, :show
    get "/HNAP1", TrollController, :show
  end

  # Other scopes may use custom stacks.
  # scope "/api", OpRankWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: OpRankWeb.Telemetry
    end
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
