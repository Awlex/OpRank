defmodule OpRankWeb.LayoutView do
  use OpRankWeb, :view

  # Phoenix LiveDashboard is available only in development by default,
  # so we instruct Elixir to not warn if the dashboard route is missing.
  @compile {:no_warn_undefined, {Routes, :live_dashboard_path, 2}}

  def active(conn, module) do
    controller =
      (conn.private[:phoenix_controller] || elem(conn.private.phoenix_live_view, 0))
      |> Module.split()
      |> tl()
      |> Module.concat()

    if controller == module do
      "btn-link"
    end
  end
end
