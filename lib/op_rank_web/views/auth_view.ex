defmodule OpRankWeb.AuthView do
  use OpRankWeb, :view

  def auth_buttons(assigns) do
    ~H"""
    <a href={Routes.auth_path(@conn, :request, "discord")} class="btn btn-primary">
        <span class="normal-case">Sign in with Discord</span>
    </a>
    """
  end
end
