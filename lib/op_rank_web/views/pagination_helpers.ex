defmodule OpRankWeb.PaginationHelpers do
  import Phoenix.LiveView
  import Phoenix.LiveView.Helpers, only: [sigil_H: 2]
  use Phoenix.HTML

  def pagination(assigns) do
    %Plug.Conn{request_path: request_path, params: params} = conn = assigns.conn
    current_page = parse_page(conn)
    max_numbers = Map.get(assigns, :max_numbers, 8)
    pages = generate_pages(assigns.page, current_page, max_numbers)

    ~H"""
    <div class="btn-group">
      <%= for {text, page} <- pages do %>
        <%= link text, to: page_path(request_path, params, page), class: "btn btn-sm #{current_page == page && "btn-active btn-disabled"}" %>
      <% end %>
    </div>
    """
  end

  def live_pagination(assigns) do
    assigns = assign_new(assigns, :max_numbers, fn -> 8 end)

    ~H"""
    <div class="btn-group">
      <%= for {text, page} <- generate_pages(@page, @current_page, @max_numbers) do %>
        <span class={"btn btn-sm #{@current_page == page && "btn-active btn-disabled"}"} phx-click="set_page" phx-value-page={page}><%= text %></span>
      <% end %>
    </div>
    """
  end

  def on_mount(:default, _, _, socket) do
    socket =
      socket
      |> assign(:page, 1)
      |> assign(:page_size, 10)
      |> attach_hook(:pagination_handler, :handle_event, fn
        "set_page", %{"page" => page}, socket ->
          {:cont, assign(socket, :page, String.to_integer(page))}

        _, _, socket ->
          {:cont, socket}
      end)

    {:cont, socket}
  end

  defp page_path(req_path, params, page) do
    params = Map.put(params, "page", page)

    URI.parse(req_path)
    |> Map.put(:query, URI.encode_query(params))
    |> to_string()
  end

  defp generate_pages(page, 1, max_numbers) do
    pages = min(page.total_pages, max_numbers)

    1..pages
    |> Enum.map(&{to_string(&1), &1})
    |> Enum.concat([{"Next", 2}])
  end

  defp generate_pages(page, current_page, max_numbers) when page.total_pages == current_page do
    min_page = max(page.total_pages - max_numbers, 1)

    [
      {"Previous", page.total_pages - 1}
      | Enum.map(min_page..page.total_pages, &{to_string(&1), &1})
    ]
  end

  defp generate_pages(page, current_page, max_numbers) when current_page < div(max_numbers, 2) do
    pages = min(page.total_pages, max_numbers)

    List.flatten([
      {"Previous", current_page - 1},
      Enum.map(1..pages, &{to_string(&1), &1}),
      {"Next", current_page + 1}
    ])
  end

  defp generate_pages(page, current_page, max_numbers)
       when current_page > page.total_pages - div(max_numbers, 2) do
    min_page = max(page.total_pages - max_numbers, 1)

    List.flatten([
      {"Previous", current_page - 1},
      Enum.map(min_page..page.total_pages, &{to_string(&1), &1}),
      {"Next", current_page + 1}
    ])
  end

  defp generate_pages(_, current_page, max_number) do
    extra = rem(max_number, 2)
    half = div(max_number, 2)

    min = current_page - half
    max = current_page + half + extra

    List.flatten([
      {"Previous", current_page - 1},
      Enum.map(min..max, &{to_string(&1), &1}),
      {"Next", current_page + 1}
    ])
  end

  defp parse_page(conn) do
    with {:ok, page} <- Map.fetch(conn.params, "page"),
         {parsed, _} when parsed > 1 <- Integer.parse(page) do
      parsed
    else
      _ -> 1
    end
  end
end
