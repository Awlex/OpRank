defmodule OpRankWeb.StringHelpers do
  def surround(nil), do: nil
  def surround(name), do: ~s("#{name}")
end
