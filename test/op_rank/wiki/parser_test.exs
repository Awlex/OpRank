defmodule OpRank.Wiki.ParserTest do
  use OpRank.DataCase

  alias OpRank.Wiki.Parser

  describe "year_index.md" do
    test "returns years from index" do
      data = File.read!("test/data/year_index.md")
      years = Parser.parse_years(data)
      assert "/r/AnimeThemes/wiki/2021" in years
      assert "/r/AnimeThemes/wiki/2020" in years
      assert "/r/AnimeThemes/wiki/2001" in years
      assert "/r/AnimeThemes/wiki/90s" in years
      assert "/r/AnimeThemes/wiki/60s" in years
    end
  end

  describe "year.md" do
    test "parses file" do
      data = File.read!("test/data/2020.md")

      Parser.parse_year(data)
    end

    test "jjk" do
      data = File.read!("test/data/jjk.md")

      anime = Parser.parse_year(data)
      assert Enum.count(anime) > 0
      assert jjk = Enum.find(anime, &(&1.name == "Jujutsu Kaisen"))
      assert jjk.mal_id == 40748
      assert jjk.season == :fall
      assert jjk.year == 2020
      assert Enum.count(jjk.themes) == 4

      [op1, op2] = Enum.filter(jjk.themes, &(&1.type == :op))
      assert Enum.count(op1.versions) == 4
      assert op1.index < op2.index
      assert op1.name == "Kaikai Kitan"
      assert Enum.count(hd(op1.versions).urls) == 2

      [a, b | _] = op1.versions
      assert a.version < b.version
      assert hd(hd(op1.versions).urls).name == "Webm"
      assert hd(hd(op1.versions).urls).url
    end

    test "higurashi" do
      data = File.read!("test/data/higurashi_ni_rei.md")

      anime = Parser.parse_year(data)
      assert Enum.count(anime) > 0
      assert higurashi = Enum.find(anime, &(&1.name =~ "Higurashi"))
      assert higurashi.mal_id == 3652
      assert higurashi.season == :fall
      assert higurashi.year == 2020
      assert Enum.count(higurashi.themes) == 2

      [ed, op] = higurashi.themes
      assert Enum.count(op.versions) == 1
      assert Enum.count(ed.versions) == 1
      assert op.name == "Super Scription of Data"
      assert ed.name == "Manazashi"
    end
  end
end
