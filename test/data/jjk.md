##2020 Fall Season (4th Quarter)
###[Jujutsu Kaisen](https://myanimelist.net/anime/40748/)

Theme title|Links|Episodes|Notes
-|:-:|:-:|:-:|:-:|:-:
OP1 V1 "Kaikai Kitan"|[Webm](https://animethemes.moe/video/JujutsuKaisen-OP1.webm)|1-2|
||[Webm \(NC, BD, 1080)](https://animethemes.moe/video/JujutsuKaisen-OP1-NCBD1080.webm)||
OP1 V2 "Kaikai Kitan"|[Webm](https://animethemes.moe/video/JujutsuKaisen-OP1v2.webm)|3-5|
||[Webm \(NC, BD, 1080)](https://animethemes.moe/video/JujutsuKaisen-OP1v2-NCBD1080.webm)||
OP1 V3 "Kaikai Kitan"|[Webm](https://animethemes.moe/video/JujutsuKaisen-OP1v3.webm)|6-7|
||[Webm \(NC, BD, 1080)](https://animethemes.moe/video/JujutsuKaisen-OP1v3-NCBD1080.webm)||
OP1 V4 "Kaikai Kitan"|[Webm](https://animethemes.moe/video/JujutsuKaisen-OP1v4.webm)|8-13|
||[Webm \(NC, BD, 1080)](https://animethemes.moe/video/JujutsuKaisen-OP1v4-NCBD1080.webm)||
OP2 "VIVID VICE"|[Webm](https://animethemes.moe/video/JujutsuKaisen-OP2.webm)|14-24|
||[Webm \(NC, BD, 1080)](https://animethemes.moe/video/JujutsuKaisen-OP2-NCBD1080.webm)||
ED1 "LOST IN PARADISE feat. AKLO"|[Webm](https://animethemes.moe/video/JujutsuKaisen-ED1.webm)|2-13|
||[Webm \(NC, BD, 1080)](https://animethemes.moe/video/JujutsuKaisen-ED1-NCBD1080.webm)||
ED2 "give it back"|[Webm](https://animethemes.moe/video/JujutsuKaisen-ED2.webm)|14-24|
||[Webm \(NC, BD, 1080)](https://animethemes.moe/video/JujutsuKaisen-ED2-NCBD1080.webm)||