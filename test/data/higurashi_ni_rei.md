##2020 Fall Season (4th Quarter)
###[Higurashi no Naku Koro ni Rei](https://myanimelist.net/anime/3652/)
**When They Cry: Rei, Higurashi no Naku Koro ni 3rd Season, Higurashi no Naku Koro ni Dai San Ki**

Theme title|Links|Episodes|Notes
-|:-:|:-:|:-:|:-:|:-:
OP "Super Scription of Data"|[Webm \(NC, BD, 1080)](https://animethemes.moe/video/HigurashiRei-OP1.webm)||
ED "Manazashi"|[Webm \(NC, BD, 1080)](https://animethemes.moe/video/HigurashiRei-ED1.webm)||