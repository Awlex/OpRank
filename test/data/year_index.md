**Please report any dead links or mistakes [to the moderators](/message/compose?to=%2Fr%2FAnimeThemes).**

---

###[2021](/r/AnimeThemes/wiki/2021) - last updated October 30 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2021 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2021#wiki_2021_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2021 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2021#wiki_2021_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2021 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2021#wiki_2021_winter_season_.281st_quarter.29)**

###[2020](/r/AnimeThemes/wiki/2020) - last updated October 30 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2020 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2020#wiki_2020_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2020 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2020#wiki_2020_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2020 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2020#wiki_2020_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2020 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2020#wiki_2020_winter_season_.281st_quarter.29)**

###[2019](/r/AnimeThemes/wiki/2019) - last updated October 30 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2019 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2019#wiki_2019_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2019 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2019#wiki_2019_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2019 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2019#wiki_2019_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2019 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2019#wiki_2019_winter_season_.281st_quarter.29)**

###[2018](/r/AnimeThemes/wiki/2018) - last updated September 8 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2018 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2018#wiki_2018_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2018 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2018#wiki_2018_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2018 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2018#wiki_2018_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2018 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2018#wiki_2018_winter_season_.281st_quarter.29)**

###[2017](/r/AnimeThemes/wiki/2017) - last updated October 10 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2017 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2017#wiki_2017_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2017 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2017#wiki_2017_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2017 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2017#wiki_2017_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2017 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2017#wiki_2017_winter_season_.281st_quarter.29)**

###[2016](/r/AnimeThemes/wiki/2016) - last updated September 11 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2016 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2016#wiki_2016_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2016 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2016#wiki_2016_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2016 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2016#wiki_2016_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2016 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2016#wiki_2016_winter_season_.281st_quarter.29)**

###[2015](/r/AnimeThemes/wiki/2015) - last updated October 8 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2015 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2015#wiki_2015_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2015 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2015#wiki_2015_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2015 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2015#wiki_2015_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2015 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2015#wiki_2015_winter_season_anime_.281st_quarter.29)**

###[2014](/r/AnimeThemes/wiki/2014) - last updated August 13 2021
**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2014 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2014#wiki_2014_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2014 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2014#wiki_2014_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2014 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2014#wiki_2014_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2014 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2014#wiki_2014_winter_season_.281st_quarter.29)**

###[2013](/r/AnimeThemes/wiki/2013) - last updated October 22 2021
**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2013 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2013#wiki_2013_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2013 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2013#wiki_2013_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2013 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2013#wiki_2013_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2013 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2013#wiki_2013_winter_season_.281st_quarter.29)**

###[2012](/r/AnimeThemes/wiki/2012)  - last updated August 18 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2012 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2012#wiki_2012_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2012 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2012#wiki_2012_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2012 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2012#wiki_2012_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2012 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2012#wiki_2012_winter_season_.281st_quarter.29)**

###[2011](/r/AnimeThemes/wiki/2011) - last updated October 17 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2011 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2011#wiki_2011_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2011 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2011#wiki_2011_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2011 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2011#wiki_2011_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2011 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2011#wiki_2011_winter_season_.281st_quarter.29)**

###[2010](/r/AnimeThemes/wiki/2010) - last updated October 22 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2010 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2010#wiki_2010_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2010 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2010#wiki_2010_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2010 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2010#wiki_2010_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2010 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2010#wiki_2010_winter_season_.281st_quarter.29)**

###[2009](/r/AnimeThemes/wiki/2009) - last updated October 12 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2009 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2009#wiki_2009_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2009 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2009#wiki_2009_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2009 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2009#wiki_2009_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2009 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2009#wiki_2009_winter_season_.281st_quarter.29)**

###[2008](/r/AnimeThemes/wiki/2008) - last updated October 25 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2008 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2008#wiki_2008_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2008 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2008#wiki_2008_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2008 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2008#wiki_2008_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2008 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2008#wiki_2008_winter_season_.281st_quarter.29)**

###[2007](/r/AnimeThemes/wiki/2007) - last updated September 23 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2007 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2007#wiki_2007_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2007 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2007#wiki_2007_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2007 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2007#wiki_2007_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2007 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2007#wiki_2007_winter_season_.281st_quarter.29)**

###[2006](/r/AnimeThemes/wiki/2006) - last updated September 16 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2006 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2006#wiki_2006_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2006 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2006#wiki_2006_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2006 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2006#wiki_2006_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2006 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2006#wiki_2006_winter_season_.281st_quarter.29)**

###[2005](/r/AnimeThemes/wiki/2005) - last updated September 11 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2005 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2005#wiki_2005_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2005 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2005#wiki_2005_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2005 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2005#wiki_2005_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2005 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2005#wiki_2005_winter_season_.281st_quarter.29)**

###[2004](/r/AnimeThemes/wiki/2004) - last updated August 26 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2004 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2004#wiki_2004_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2004 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2004#wiki_2004_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2004 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2004#wiki_2004_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2004 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2004#wiki_2004_winter_season_.281st_quarter.29)**

###[2003](/r/AnimeThemes/wiki/2003) - last updated September 30 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2003 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2003#wiki_2003_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2003 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2003#wiki_2003_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2003 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2003#wiki_2003_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2003 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2003#wiki_2003_winter_season_.281st_quarter.29)**

###[2002](/r/AnimeThemes/wiki/2002) - last updated September 30 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2002 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2002#wiki_2002_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2002 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2002#wiki_2002_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2002 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2002#wiki_2002_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2002 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2002#wiki_2002_winter_season_.281st_quarter.29)**

###[2001](/r/AnimeThemes/wiki/2001) - last updated October 9 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2001 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2001#wiki_2001_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2001 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2001#wiki_2001_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2001 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2001#wiki_2001_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2001 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2001#wiki_2001_winter_season_.281st_quarter.29)**

###[2000](/r/AnimeThemes/wiki/2000) - last updated September 16 2021

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2000 Fall Season (4th Quarter)](/r/AnimeThemes/wiki/2000#wiki_2000_fall_season_.284th_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2000 Summer Season (3rd Quarter)](/r/AnimeThemes/wiki/2000#wiki_2000_summer_season_.283rd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2000 Spring Season (2nd Quarter)](/r/AnimeThemes/wiki/2000#wiki_2000_spring_season_.282nd_quarter.29)**

**&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;[2000 Winter Season (1st Quarter)](/r/AnimeThemes/wiki/2000#wiki_2000_winter_season_.281st_quarter.29)**

###[1990 - 1999](/r/AnimeThemes/wiki/90s) - last updated October 11 2021

###[1980 - 1989](/r/AnimeThemes/wiki/80s) - last updated October 9 2021

###[1970 - 1979](/r/AnimeThemes/wiki/70s) - last updated September 13 2021

###[1960 - 1969](/r/AnimeThemes/wiki/60s) - last updated October 10 2021